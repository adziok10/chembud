import $ from "jquery";
import "slick-carousel";

$('.js-opinion-carousel').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          arrows: false,
        }
      }
    ]
  });