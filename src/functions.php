<?php 
if( !defined('ABSPATH') ) { die('No direct access'); }

require_once get_template_directory() . '/lib/base.php';


function copyrightyear( $start = 0 ) {
    $now = date('Y');
    if(!$start) { $start = $now; }

    if( $start != $now ) {
        echo $start . '-' . $now;
    } else {
        echo $start;
    }
}
