<?php if( !defined('ABSPATH') ) { die('No direct access'); } ?>
<!DOCTYPE html>
<html <?php echo language_attributes(); ?>>

<head>
    <meta http-equiv="content-type" content="text/html; charset=<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="google-site-verification" content="5RMY860_DOj1aLeRrAWj-6VMeO05U9wgN2TRjWu3NzY" />
    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
    <link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
    <link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <title>
        <?php bloginfo('name'); ?>
    </title>

    <?php wp_head(); ?>
</head>

<body <?php echo body_class(); ?>>

    <nav class="o-sticky-nav c-nav-top container-fluid">
        <div class="row">
            <div class="col-lg-2 offset-lg-2">
                <div class="o-sticky-nav__logo-wrapper c-nav-top__logo-wrapper">
                    <a href="">
                        <img src="<?=IMAGES_URI?>logo.svg" class="o-sticky-nav__img c-nav-top__logo" alt="logo">
                    </a>
                </div>
            </div>
            <div class="col-lg-8 o-sticky-nav__items-wrapper c-nav-top__items-wrapper">
                    <a href="#" class="o-sticky-nav__link c-nav-top__link">Strona główna</a>
                    <a href="#" class="o-sticky-nav__link c-nav-top__link">O firmie</a>
                    <a href="#" class="o-sticky-nav__link c-nav-top__link">Oferta</a>
                    <a href="#" class="o-sticky-nav__link c-nav-top__link">Zapytanie ofertowe</a>
                    <a href="#" class="o-sticky-nav__link c-nav-top__link">Kontakt</a>
            </div>
        </div>
    </nav>