<?php if( !defined('ABSPATH') ) { die('No direct access'); }
/**
 * Template Name: Strona główna
 * 
 */
get_header(); ?>

<header class="container-fluid o-header c-header-main">
    <div class="row">
        <div class="col-lg-7 o-header__left-container c-header-main__left-container">
            <div class="row">
                <div class="col-lg-8 offset-lg-4 col-md-10 offset-md-1">
                    <h1 class="o-header__title c-header-main__title">
                        Preofesionalne posadzki
                    </h1>
                    <p class="o-header__sub-title c-header-main__sub-title">
                        Zajmujemy się profesjonalnym wykonawstwem posadzek przemysłowych i dekoracyjnych z żywic.
                    </p>
                    <p class="o-header__sub-title o-header__sub-title--margin c-header-main__sub-title">
                        Jesteśmy autoryzowanym wykonawcą posadzek przemysłowych firm Silikal, BASF, Sika, Sto,
                        Techniart.
                    </p>
                </div>
            </div>
        </div>
        <div class="col-lg-5 o-header__right-container c-header-main__right-container">

        </div>
    </div>
</header>

<section class="o-two-container c-about container-fluid">
    <div class="row">
        <div class="col-lg-7 o-two-container__left">
            <div class="row">
                <div class="col-md-8 offset-md-2 col-lg-7 offset-lg-4">
                    <h2 class="o-two-container__title c-about__title">
                        O firmie
                    </h2>
                    <p class="o-two-container__sub-title c-about__sub-title">
                        Firma <strong class="c-about__important-text">Chembud</strong> powstała w 1991 roku.
                        Specjalizujemy się w przemysłowych posadzkach żywicznych.
                    </p>
                    <p class="o-two-container__text c-about__text">
                        Założyciel firmy Jan Jakimiuk od 1965 r. brał czynny udział we wprowadzaniu posadzek żywicznych
                        do budownictwa. Doświadczenie zdobyte przez wiele lat pracy nad wszelkimi systemami posadzek
                        procentuje do dzisiaj.
                    </p>
                    <a href="#" class="o-two-container__btn c-about__btn">Więcej</a>
                </div>
            </div>
        </div>
        <div class="col-lg-5 o-two-container__right c-about__right">
            <div class="o-two-container__img-wrapper c-about__img-wrapper">
                <img src="<?=IMAGES_URI?>about-img.png" alt="a" class="o-two-container__img c-about__img">
            </div>
        </div>
    </div>
</section>

<section class="container-fluid o-offer c-offer">
    <div class="row">
        <div class="col-lg-3 col-sm-6">
            <div class="o-offer__item c-offer__item">
                <img src="<?=IMAGES_URI?>offer-img.png" alt="bf">
                <div class="o-offer__desc-wrapper c-offer__desc-wrapper">
                    <div class="o-offer__desc c-offer__desc">
                        Posadzki epoksydowe i poliuretanowe
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="o-offer__item c-offer__item">
                <img src="<?=IMAGES_URI?>offer-img.png" alt="bf">
                <div class="o-offer__desc-wrapper c-offer__desc-wrapper">
                    <div class="o-offer__desc c-offer__desc">
                        Posadzki epoksydowe i poliuretanowe
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="o-offer__item c-offer__item">
                <img src="<?=IMAGES_URI?>offer-img.png" alt="bf">
                <div class="o-offer__desc-wrapper c-offer__desc-wrapper">
                    <div class="o-offer__desc c-offer__desc">
                        Posadzki epoksydowe i poliuretanowe
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="o-offer__item c-offer__item">
                <img src="<?=IMAGES_URI?>offer-img.png" alt="bf">
                <div class="o-offer__desc-wrapper c-offer__desc-wrapper">
                    <div class="o-offer__desc c-offer__desc">
                        Posadzki epoksydowe i poliuretanowe
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid o-opinions c-opinions">
    <div class="container">
        <div class="row">
            <div class="col-12 c-opinions__quotes">„</div>
            <div class="col-sm-8 offset-sm-2 js-opinion-carousel">
                <div class="o-opinions__wrapper c-opinions__wrapper">
                    <h3 class="o-opinions__title c-opinions__title">Chembud należy do zdecydowanej czołówki jeśli
                        chodzi o swój fach.</h3>
                    <p class="o-opinions__content c-opinions__content">Lorem ipsum dolor sit amet, consectetur
                        adipiscing elit. Phasellus et sagittis quam, dignissim ullamcorper nunc. Nam et luctus massa.
                        Sed id accumsan tortor, in vehicula urna. Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit. Donec egestas nibh id purus suscipit, a ullamcorper ipsum consectetur.</p>
                    <p class="o-opinions__author c-opinions__author"><span class="c-opinions__name">Jan Kowalski</span>
                        – prezes Fakro</p>
                </div>
                <div class="o-opinions__wrapper c-opinions__wrapper">
                    <h3 class="o-opinions__title c-opinions__title">Chembud należy do zdecydowanej czołówki jeśli
                        chodzi o swój fach.</h3>
                    <p class="o-opinions__content c-opinions__content">Lorem ipsum dolor sit amet, consectetur
                        adipiscing elit. Phasellus et sagittis quam, dignissim ullamcorper nunc. Nam et luctus massa.
                        Sed id accumsan tortor, in vehicula urna. Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit. Donec egestas nibh id purus suscipit, a ullamcorper ipsum consectetur.</p>
                    <p class="o-opinions__author c-opinions__author"><span class="c-opinions__name">Jan Kowalski</span>
                        – prezes Fakro</p>
                </div>
                <div class="o-opinions__wrapper c-opinions__wrapper">
                    <h3 class="o-opinions__title c-opinions__title">Chembud należy do zdecydowanej czołówki jeśli
                        chodzi o swój fach.</h3>
                    <p class="o-opinions__content c-opinions__content">Lorem ipsum dolor sit amet, consectetur
                        adipiscing elit. Phasellus et sagittis quam, dignissim ullamcorper nunc. Nam et luctus massa.
                        Sed id accumsan tortor, in vehicula urna. Lorem ipsum dolor sit amet, consectetur adipiscing
                        elit. Donec egestas nibh id purus suscipit, a ullamcorper ipsum consectetur.</p>
                    <p class="o-opinions__author c-opinions__author"><span class="c-opinions__name">Jan Kowalski</span>
                        – prezes Fakro</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="o-companies c-companies container">
    <div class="row">
        <div class="col-12">
            <img src="<?=IMAGES_URI?>companies.img.png" alt="fas" class="o-companies__img c-companies__img">
        </div>
    </div>
</section>

<section class="container-fluid o-contact c-contact">
    <div class="row row-eq-height">
        <div class="col-lg-6 o-two-container__left">
            <div class="o-two-container__map">
                <img src="<?=IMAGES_URI?>map.png" alt="as">
            </div>
        </div>
        <div class="col-lg-6 o-two-container__right c-about__righte">
            <div class="o-two-container__img-wrapper c-about__img-wrapper">
                <div class="c-contact-form o-contact-form">
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="text" class="c-contact-form__input o-contact-form__input" placeholder="Imię">
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="c-contact-form__input o-contact-form__input" placeholder="Nazwisko">
                        </div>
                        <div class="col-sm-6">
                            <input type="mail" class="c-contact-form__input o-contact-form__input" placeholder="Adres e-mail">
                        </div>
                        <div class="col-sm-6">
                            <input type="tel" class="c-contact-form__input o-contact-form__input" placeholder="Numer telefonu">
                        </div>
                        <div class="col-12">
                            <textarea class="c-contact-form__textarea o-contact-form__textarea" placeholder="Wiadomość"></textarea>
                        </div>
                        <div class="col-12">
                            <button class="c-contact-form__btn o-contact-form__btn">Wyślij</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer();