//const path = require('path');

module.exports = {
    //   entry: './src/app.js',
    output: {
        filename: 'main.js'
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /(node_modules)/,
            use: {
                loader: "babel-loader"
            }
        }]
    },
    mode: 'production'
};