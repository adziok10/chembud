<?php if (!defined('ABSPATH')) {die('No direct access');}?>

<footer class="container-fluid c-footer o-footer">
        <div class="container">
                <div class="row">
                        <div class="col-lg-3 c-footer__about o-footer__about">
                                <img src="<?=IMAGES_URI?>logo-alt.svg" alt="logosa"><br>
                                CHEMBUD ARTUR JAKIMIUK<br>
                                2018 Copyright © CHEMBUD
                        </div>
                        <div class="col-lg-3 c-footer__contact o-footer__contact">
                                Kontakt: <br>
                                Tel. 602-657-303 <br>
                                Fax. 22 758-90-61 <br>
                                e-mail: biuro@chembud.com
                        </div>
                        <div class="col-lg-6">
                                <div class="c-footer__create-by o-footer__create-by">
                                        <img src="<?=IMAGES_URI?>apollo.png" alt="aassd">
                                </div>
                        </div>
                </div>
        </div>
</footer>

<?php wp_footer();?>

</body>

</html>