const gulp = require('gulp');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean-css');
const webpack = require('webpack-stream');
const browserSync = require('browser-sync');
const del = require('del');

const sync = browserSync.create();
const reload = sync.reload;
const config = {
    paths: {
        src: {
            html: './src/**/*.html',
            php: './src/**/*.php',
            img: './src/img/**.*',
            sass: [
                'src/sass/style.scss',
                'src/sass/fold.scss'
            ],
            js: 'src/js/app.js',
            cssMain: 'src/style.css'
        },
        dist: {
            main: './chemex',
            css: './chemex/assets/styles',
            js: './chemex/assets/js',
            img: './chemex/assets/images'
        }
    }
};

gulp.task('js', () => {
    return gulp.src(config.paths.src.js)
        .pipe(webpack(require('./webpack.config.js')))
        .pipe(gulp.dest(config.paths.dist.js));

       // reload();
});

gulp.task('sass', () => {
    return gulp.src(config.paths.src.sass)
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(clean())
        .pipe(gulp.dest(config.paths.dist.css))
        //.pipe(sync.stream());
});

gulp.task('static', () => {
    gulp.src(config.paths.src.html)
        .pipe(gulp.dest(config.paths.dist.main));

    gulp.src(config.paths.src.php)
        .pipe(gulp.dest(config.paths.dist.main));

    gulp.src(config.paths.src.img)
        .pipe(gulp.dest(config.paths.dist.img));

    gulp.src(config.paths.src.cssMain)
        .pipe(gulp.dest(config.paths.dist.main));

    //reload();
});

gulp.task('clean', () => {
    return del([config.paths.dist.main]);
});

gulp.task('build', ['clean'], function() {
    gulp.start( 'sass','js', 'static');
});

gulp.task('server', () => {
    sync.init({
        injectChanges: true,
        server: config.paths.dist.main,
        //proxy: 'http://localhost/wordpressenv/'
    });
});

gulp.task('watch', ['default'], function() {
    gulp.watch('src/sass/**/*.scss', ['sass']);
    gulp.watch('src/js/**/*.js', ['js']);
    gulp.watch('src/*.html', ['static']);
    gulp.watch('src/*.php', ['static']);
    //gulp.start('server');
});

gulp.task('default', ['build']);